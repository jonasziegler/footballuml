package at.zie.footballTeam;

import java.util.Date;

public class Person implements NewsletterReceiver{
	private String name;
	private Date birthday;
	
	public Person(String name, Date birthday) {
		super();
		this.name = name;
		this.birthday = birthday;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	@Override
	public void sendInfo(String msg) {
		// TODO Auto-generated method stub
		System.out.println(this.name +": " + msg);
	}
	
	
}
