package at.zie.footballTeam;

import java.util.Date;

public class Goalkeeper extends Player {
	private int reactionTime;

	public Goalkeeper(String name, Date birthday, int playerValue, int reactionTime) {
		super(name, birthday, playerValue);
		this.reactionTime = reactionTime;
	}

	public int getReactionTime() {
		return reactionTime;
	}

	public void setReactionTime(int reactionTime) {
		this.reactionTime = reactionTime;
	}
	
	

}
