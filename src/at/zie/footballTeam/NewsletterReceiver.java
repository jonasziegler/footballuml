package at.zie.footballTeam;

public interface NewsletterReceiver {
	public void sendInfo(String msg);
}
