package at.zie.footballTeam;

import java.util.Date;

public class Player extends Person {
	private int playerValue;

	public Player(String name, Date birthday, int playerValue) {
		super(name, birthday);
		this.playerValue = playerValue;
	}

	public int getPlayerValue() {
		return playerValue;
	}

	public void setPlayerValue(int playerValue) {
		this.playerValue = playerValue;
	}
	

}
