package at.zie.footballTeam;

import java.util.ArrayList;
import java.util.List;

public class NewsletterManager {
	private List<NewsletterReceiver> receiverList;

	public NewsletterManager() {
		super();
		this.receiverList = new ArrayList<NewsletterReceiver>();
	}
	
	public void addReceiver(Person personName ) {
		receiverList.add(personName);
	}
	
	public void sendInfo(String msg) {
		for (NewsletterReceiver newsletterReceiver : receiverList) {
			newsletterReceiver.sendInfo(msg);
		}
	}
	
}
