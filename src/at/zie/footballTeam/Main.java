package at.zie.footballTeam;

import java.util.Date;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Player fp1 = new Player("Tobi", new Date(), 5);
		Goalkeeper gk1 = new Goalkeeper("Luca", new Date(), 5, 3);
			
		Team t1 = new Team("cWI");
		t1.addPlayer(fp1);
		t1.addPlayer(gk1);
		
		System.out.println(t1.getValueOfTeam());
		
		NewsletterManager nm = new NewsletterManager();
		
		nm.addReceiver(fp1);
		nm.addReceiver(gk1);
		
		nm.sendInfo("neues Trikot");
	}

}
