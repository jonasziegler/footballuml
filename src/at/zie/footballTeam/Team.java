package at.zie.footballTeam;

import java.util.ArrayList;
import java.util.List;

public class Team {
	private String name;
	private List<Player> playerList = new ArrayList<>();
			
	public Team(String name) {
		super();
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public  int getValueOfTeam() {
		int value = 0;
		for (Player player : playerList) {
			value += player.getPlayerValue();
		}
		return value;
	}
	
	public void addPlayer(Player playerName) {
		playerList.add(playerName);
	}
}
